<?php


include('configDB.php');


function debug ($text)
{  
  echo $text . "<br>\n";
}
?>
<head>
  <title>Bible Reading</title>

  <meta name="robots" content="noindex">
	<meta content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" name="viewport">
	<meta content="yes" name="apple-mobile-web-app-capable">

  <link rel="stylesheet" type="text/css" href="styles.css">
</head>
  <body> 
    <div class="esv">

<?php



//example
//Phil+1-4;+Mark+2:1-12;+Ps+43;+Pr+10:10

$verses = $_GET['text'];

$Bible_version = "esv";


$reference = explode(";", $verses);


for ($a = 0; $a < count($reference); $a++)
{

  $book = $start_chapter= $finish_chapter= $start_verses= $finish_verses = "0";


  //echo "<h2>" . trim($reference[$a]) . "</h2>";
  $book_split = explode(" ", trim($reference[$a]));


  if (($book_split[0] == "1") ||($book_split[0] == "2") || ($book_split[0] == "3"))
  {
    //echo "heelo";
    $book_split[0] = $book_split[0] . " " . $book_split[1];
    $book_split[1] = $book_split[2]; // . " " . $book_split[2] ;
    
    //echo $book_split[0];
    //echo $book_split[1]; 

  }




  //  debug ( count($book_split) );

  //echo "<b>book name: " . $book_split[0] . "</b>";
  //echo "&nbsp;&nbsp;&nbsp;&nbsp;  reference: " . $book_split[1];

  $book = $book_split[0];
  $references = $book_split[1];
  

  $references_array = explode(":", trim($references));

  //debug ( " - " . count($references_array) );
  //debug ( "<br>" );



  switch ( count($references_array)) 
  {
    case 0:
      // No reference.
      // We don't list the full book. Ignoring.


 


    case 1:
      //code to be executed if n=label1;      
      //debug ( "full chapter reference" );
      
      $references_2nd_array_counts = explode("-", trim($references));
 
      if(references_2nd_array_counts==1)
      {
        // for a single chapter reference.

        $references_2nd_array = explode(":", trim($references));

        $chapter_start = $references_2nd_array[0]; // 
        $chapter_end = $chapter_start;
        //$verse_start = $references_3rd_array_start[1]; // 
      
        $verse_start = 0; // 
        $verse_end = 0; // 

        //echo $book . " - " . $chapter_start . " - " . $chapter_end . " - " . $verse_start . " - " . $verse_end . " - " . trim($reference);
        $special = 0;
        get_bible_verses_v2 ($dbc, $book, $chapter_start, $chapter_end, $verse_start, $verse_end, trim($references), $special);
      }
      else
      {

        // if it goes over multiple chapters.
          $references_2nd_array = explode("-", trim($references));

          if(count($references_2nd_array) == 1)
          {
            $chapter_start = $references_2nd_array[0]; // 
            $chapter_end = $references_2nd_array[0];
          }
          else
          {
            $chapter_start = $references_2nd_array[0]; // 
            $chapter_end = $references_2nd_array[1];
          }
        
          $verse_start = 0; // 
          $verse_end = 0; // 
  
          //echo $book . " - " . $chapter_start . " - " . $chapter_end . " - " . $verse_start . " - " . $verse_end . " - " . trim($reference);
          $special = 0;
          get_bible_verses_v2 ($dbc, $book, $chapter_start, $chapter_end, $verse_start, $verse_end, trim($references), $special);
      }


      

      break;
    case 2:
      //code to be executed if n=label2;
      //debug ( "chapter and verse to verse  reference" );


      
      $references_2nd_array = explode(":", trim($references)); 

      if (count($references_2nd_array) == 1)
      {
        
        $chapter_start = $references_2nd_array[0]; // 
        $chapter_end = $chapter_start;
        //$verse_start = $references_3rd_array_start[1]; // 

        $references_3rd_array_finish = explode("-", trim($references_2nd_array[1]));
        
        $verse_start = $references_3rd_array_finish[0]; // 
        $verse_end = $references_3rd_array_finish[1]; // 

        //echo $book . " - " . $chapter_start . " - " . $chapter_end . " - " . $verse_start . " - " . $verse_end . " - " . trim($reference);
        $special = 0;
        get_bible_verses_v2 ($dbc, $book, $chapter_start, $chapter_end, $verse_start, $verse_end, trim($references), $special);
      }
      else
      {
        $chapter_start = $references_2nd_array[0]; // 
        $chapter_end = $chapter_start;
        //$verse_start = $references_3rd_array_start[1]; // 

        $references_3rd_array_finish = explode("-", trim($references_2nd_array[1]));
        
        if(count($references_3rd_array_finish ) == 1)
        {
          $verse_start = $references_2nd_array[1]; // 
          $verse_end = $references_2nd_array[1]; //   
        }
        else
        {
          $verse_start = $references_3rd_array_finish[0]; // 
          $verse_end = $references_3rd_array_finish[1]; // 
        }
 
        //echo $book . " - " . $chapter_start . " - " . $chapter_end . " - " . $verse_start . " - " . $verse_end . " - " . trim($reference);
        $special = 0;
        get_bible_verses_v2 ($dbc, $book, $chapter_start, $chapter_end, $verse_start, $verse_end, trim($references), $special);
      }




      break;
    case 3:
      //code to be executed if n=label3;
       //debug ( "chapter to chapter reference" );
      
      $references_2nd_array = explode("-", trim($references));

      //echo count($references_2nd_array);

      $references_3rd_array_start = explode(":", trim($references_2nd_array[0]));
      $chapter_start = $references_3rd_array_start[0]; // 
      $verse_start = $references_3rd_array_start[1]; // 

      $references_3rd_array_finish = explode(":", trim($references_2nd_array[1]));
      $chapter_end = $references_3rd_array_finish[0]; // 
      $verse_end = $references_3rd_array_finish[1]; // 

       //echo $book . " - " . $chapter_start . " - " . $chapter_end . " - " . $verse_start . " - " . $verse_end . " - " . trim($reference);

       $special = 1;
       get_bible_verses_v2 ($dbc, $book, $chapter_start, $chapter_end, $verse_start, $verse_end, trim($references), $special);


/*
      //Num+4:34-6:27

      AND

      t_esv.id BETWEEN 04004034 AND 04006029


*/



      break;
      
    default:
      //code to be executed if n is different from all labels;
  } 

}
 
 
 
 
function get_bible_verses_v2 ($dbc, $book, $start_chapter, $finish_chapter, $start_verses, $finish_verses, $reference, $special)
{

  //echo "start: " . $start_verses . "<br>";
  //echo "start: " . $finish_verses . "<br>";

if( $start_verses == 0)
{
  // If it's a full chapter
  $query = 'SELECT * FROM bibles.books, t_esv WHERE abbreviation = "' . addslashes($book) .'" AND
  t_esv.bookId = books.id
  and t_esv.chapterId >= ' . addslashes($start_chapter) . '
  and t_esv.chapterId <= ' . addslashes($finish_chapter); 
}
else
{
  // If it's a chapter and verse
  $query = 'SELECT * FROM bibles.books, t_esv WHERE abbreviation = "' . addslashes($book) .'" 
  AND t_esv.bookId = books.id
  and t_esv.chapterId >= ' . addslashes($start_chapter) . '
  and t_esv.chapterId <= ' . addslashes($finish_chapter) . '
  AND t_esv.verseId >= ' . addslashes($start_verses) . '
  AND t_esv.verseId <= ' . addslashes($finish_verses);
}

  //$query = "SELECT * FROM books, bibles.t_" . $Bible_version . "   where  "; 

  //debug ( $query . "<br>");
  

  $result = mysqli_query($dbc, $query);
  $num = mysqli_num_rows ($result);
  //echo $num . "<br>";

  if ($special == 1 )   
  {
    /* Try for the other rare method where the first verse reference is after the second one:
    eg: Num+4:34-6:27
    */

    $query = 'SELECT id, name FROM books  WHERE abbreviation = "' . addslashes($book) .'"';
    $result = mysqli_query($dbc, $query);
    $num = mysqli_num_rows ($result);
    if ($num > 0 ) 
    {  
      $bookid = mysqli_result($result, 0,"id"); 
      $bookName_Special = mysqli_result($result, 0,"name"); 
    }

    $startID = str_pad($bookid, 2, '0', STR_PAD_LEFT) . str_pad($start_chapter, 3, '0', STR_PAD_LEFT) . str_pad($start_verses, 3, '0', STR_PAD_LEFT);
    $stopID  = str_pad($bookid, 2, '0', STR_PAD_LEFT) . str_pad($finish_chapter, 3, '0', STR_PAD_LEFT) . str_pad($finish_verses, 3, '0', STR_PAD_LEFT);


    $query = 'SELECT * FROM  t_esv WHERE id BETWEEN ' . $startID . ' AND ' . $stopID;     
    $result = mysqli_query($dbc, $query);
    $num = mysqli_num_rows ($result);

  } 

  if ($num > 0 ) 
  {
    $i=0;
    $oldchapterId = 0;
    $book_query = "SELECT * FROM bibles.books WHERE abbreviation = '" . $book . "'";
    $book_result = mysqli_query($dbc, $book_query);
    $book_num = mysqli_num_rows ($book_result);
    if ($book_num > 0 ) 
    {  
      $fullBook = mysqli_result($book_result, 0,"name"); 
    }

    echo "<h2>" . $fullBook . " " .str_replace($book, $fullBook,  trim($reference)) . "</h2>";
    $onlyone = 0;

    echo '<div class="esv-text">'; 
    while ($i < $num) 
    {   
        

      $verse = mysqli_result($result,$i,"verse"); 
      $verseId = mysqli_result($result,$i,"verseId"); 
      $id = mysqli_result($result,$i,"id"); 
      $bookname = mysqli_result($result,$i,"name"); 
      $chapterId = mysqli_result($result,$i,"chapterId"); 

      if($bookname=="")
      {
        $bookname = $bookName_Special;
      }
   
      $relation_query = "SELECT * FROM bibles.headings WHERE book = '" . $bookname . "' AND chapter ='" . $chapterId . "' AND atverse='" . ($verseId)  ."'";
      //echo "<br>" . $relation_query ."<br>";
      
      $relation_result = mysqli_query($dbc, $relation_query);
      $relation_num = mysqli_num_rows ($relation_result);
      $heading_location = mysqli_result($relation_result, 0,"location"); 
      if ($relation_num > 0 ) 
      {  
        
        $heading = mysqli_result($relation_result, 0,"heading"); 
        $heading_location = mysqli_result($relation_result, 0,"location"); 
        if ($heading_location == 0)
        {
          echo "<h3>" . $heading . "</h3>";
        }
        echo "<p>";
      }
      else
      {
        if ($onlyone == 0)
        {
          echo "<p>";
          $onlyone = 1;
        }
      }

      

/*
  What then? Only that in every way, whether in pretense or in truth, Christ is proclaimed, and in that I rejoice. Yes, and I will rejoice, Found it   
*/
      if ($chapterId != $oldchapterId)
      {
        $verseId = $chapterId . ":" . $verseId;
      }
       
      // This is for where there is a verse missing or something else quirky.
      if ($verse != "See Footnote")
      {      
      echo '<span class="verse-num">' . $verseId . ' </span>';      
      }
      else
      {
        $verse = str_replace("See Footnote", "", $verse);
      }


      
      if ($heading_location !=0)
      {
        // This verse has a heading in the middle of it...
        $insertstr = "</p><h3>" . $heading . "</h3><p>";
        $verse = stringInsert($verse, $heading_location, $insertstr);
        echo $verse;
      }
      else
      {
        // Show verse as normal.
        echo $verse . "\n";
      }

      ++$i; 
      $oldchapterId = $chapterId;
    }
    echo "</p>\n"; 
    echo "</div>";

  } else 
  {
    //echo "The database is empty. Contact your administrator.";
  }
}







function stringInsert($str, $pos, $insertstr) {
  if (!is_array($pos)) {
      $pos = array($pos);
  } else {
      asort($pos);
  }
  $insertionLength = strlen($insertstr);
  $offset = 0;
  foreach ($pos as $p) {
      $str = substr($str, 0, $p + $offset) . $insertstr . substr($str, $p + $offset);
      $offset += $insertionLength;
  }
  return $str;
}

function mysqli_result($res,$row=0,$col=0){ 
  $numrows = mysqli_num_rows($res); 
  if ($numrows && $row <= ($numrows-1) && $row >=0){
      mysqli_data_seek($res,$row);
      $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
      if (isset($resrow[$col])){
          return $resrow[$col];
      }
  }
  return false;
} 
?>

<div id="copyright" class="esv">
		<p id="copyright_text">
			The Holy Bible, English Standard Version. Copyright ©2001 
			by <a href="http://www.crosswaybibles.org">Crossway Bibles</a>, 
			a publishing ministry of Good News Publishers. Used by permission. All rights reserved.
		</p>
	</div>
  
</div>

 </body>
 </html>