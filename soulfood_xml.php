<?php
//include('configDB.php');
 
header('Content-Type: text/xml');

/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

*/

//if (!function_exists('str_contains')) 

    function str_contains_fix( $haystack,  $needle)
    {
        return '' === $needle || false !== strpos($haystack, $needle);
    }

 
function dayofyear2date( $tDay, $tFormat = 'd-m-Y' ) {
    $day = intval( $tDay );
    $day = ( $day == 0 ) ? $day : $day - 1;
    $offset = intval( intval( $tDay ) * 86400 );
    $str = date( $tFormat, strtotime( 'Jan 1, ' . date( 'Y' ) ) + $offset );
    return( $str );
}


$myfile = fopen("2017_Soulfood.xml", "r") or die("Unable to open file!");

$old_lastday = $lastday = 0;
$compounded = "";

echo '<?xml version="1.0" encoding="UTF-8" ?><data xmlns:double="http://dashboard.thechurchapp.org/platform/feeds/schemas/double/"
        xmlns:ipad="http://dashboard.thechurchapp.org/platform/feeds/schemas/ipad/"
        xmlns:ipaddouble="http://dashboard.thechurchapp.org/platform/feeds/schemas/ipad_double/">
    <header>
		<title>Bible</title>
     </header>
        <table>
        ';

$rows = array();
 
while(! feof($myfile))  
{
    $result = fgets($myfile); 
  if (str_contains_fix($result, "<day>")) 
  {
      //echo $result;
      $lastday = str_replace("<day>", "", $result);
      $lastday = str_replace("</day>", "", $lastday);
      //echo $lastday;
      $old_lastday = $lastday;
      
      $compounded_url = str_replace(" ", '+', $compounded);
      $compounded_url = rtrim($compounded_url, ';');
      $compounded = rtrim($compounded, ';');
      if ($compounded_url != "<plan>;<title>Soulfood</title>")
      {

       if   (( date('z') + 1 ) > (int)$lastday + 40 ) 
       {
           //echo "yes";
       }
       else
       {

        if ($lastday < ( date('z') +1 ) )
        {

            /*
          echo "<row>\n";          
          echo "<date>" .  date("D, d M Y ", strtotime(dayofyear2date($lastday))) . "12:00:00 -0800 </date>\n";
          echo "<name>" .  date("l", strtotime(dayofyear2date($lastday))) . "</name>\n";          
          echo '<alternative>' . str_replace(";",", ", $compounded) . "</alternative>\n";          
          echo '<link>https://resources.vision.org.au/bibles/?text=' . $compounded_url . "</link>\n";
          echo "<handler>internalBrowser</handler>\n";          
          echo "</row>\n";
            */

          $temprow = "<row>\n";          
          $temprow = $temprow . "<date>" .  date("D, d M Y ", strtotime(dayofyear2date($lastday))) . "12:00:00 -0800 </date>\n";
          $temprow = $temprow .  "<name>" .  date("l", strtotime(dayofyear2date($lastday))) . "</name>\n";          
          $temprow = $temprow .  '<alternative>' . str_replace(";",", ", $compounded) . "</alternative>\n";          
          $temprow = $temprow .  '<link>https://resources.vision.org.au/bibles/?text=' . $compounded_url . "</link>\n";
          $temprow = $temprow .  "<handler>internalBrowser</handler>\n";          
          $temprow = $temprow .  "</row>\n";



          array_push ($rows, $temprow);  

        }


       }
      }
      $compounded = "";
      
  }
  else
  {
    if ($lastday == $old_lastday)
    {
    //echo $result;
    $snip = str_replace("\t", '', $result); // remove tabs
    $snip = str_replace("\n", '', $snip); // remove new lines
    $snip = str_replace("\r", '', $snip); // remove carriage returns

    $compounded = $compounded . $snip . ";";
    //echo $compounded;
    }
  }
} 




fclose($myfile);

krsort($rows);

foreach($rows as $file) {
    echo $file;
}

echo "</table>\n
</data>";

?> 

